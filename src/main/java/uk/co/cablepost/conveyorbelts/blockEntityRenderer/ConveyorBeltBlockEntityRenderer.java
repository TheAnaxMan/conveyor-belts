package uk.co.cablepost.conveyorbelts.blockEntityRenderer;

import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3f;
import uk.co.cablepost.conveyorbelts.block.ConveyorBelt;
import uk.co.cablepost.conveyorbelts.block.SlopedConveyorBeltUp;
import uk.co.cablepost.conveyorbelts.blockEntity.ConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.SlopedConveyorBeltDownBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.SlopedConveyorBeltUpBlockEntity;

import java.util.Objects;

public class ConveyorBeltBlockEntityRenderer<T extends ConveyorBeltBlockEntity> implements BlockEntityRenderer<T> {
    public ConveyorBeltBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
    }

    @Override
    public void render(ConveyorBeltBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {

        if(blockEntity instanceof SlopedConveyorBeltUpBlockEntity && blockEntity.getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
            return;
        }

        DefaultedList<ItemStack> beltInv = blockEntity.items;
        for(int i = 0; i < blockEntity.slotActuallyHasItem.length; i++){
            if(blockEntity.slotActuallyHasItem[i] == 0){
                beltInv.set(i, ItemStack.EMPTY);
            }
        }

        matrices.push();

        int forwardSlots = 3;
        int forwardSlotsSize = 3;

        if(blockEntity instanceof FilterConveyorBeltBlockEntity){
            forwardSlots = 4;
            forwardSlotsSize = 3;
        }

        for(int i = 0; i < forwardSlots; i++) {
            if (!beltInv.get(i).isEmpty()) {

                float cooldownOffset = getTransferCooldownOffset(blockEntity, i);
                float offset =
                        (
                                i +
                                        cooldownOffset
                        ) /
                                (float) forwardSlotsSize;

                float sidewaysOffset = (float)blockEntity.transferSidewaysOffset[i] / 100f;

                if(blockEntity instanceof FilterConveyorBeltBlockEntity filterConveyorBeltBlockEntity){
                    if (i == 2 && filterConveyorBeltBlockEntity.outputMode != FilterConveyorBeltBlockEntity.OUTPUT_MODE_LEFT_FRONT) {
                        renderBeltItem(blockEntity, matrices, vertexConsumers, light, overlay, beltInv.get(2), 0.5f, -getTransferCooldownOffset(blockEntity, 2) / 2);
                        continue;
                    }
                    if (i == 3 && filterConveyorBeltBlockEntity.outputMode != FilterConveyorBeltBlockEntity.OUTPUT_MODE_RIGHT_FRONT) {
                        renderBeltItem(blockEntity, matrices, vertexConsumers, light, overlay, beltInv.get(3), 0.5f, getTransferCooldownOffset(blockEntity, 3) / 2);
                        continue;
                    }

                    if(i >= 2){
                        //offset += 0.25f;
                    }
                }

                renderBeltItem(blockEntity, matrices, vertexConsumers, light, overlay, beltInv.get(i), offset, sidewaysOffset);
            }
        }

        /*
        if(blockEntity instanceof FilterConveyorBeltBlockEntity){
            if (!beltInv.get(2).isEmpty()) {
                renderBeltItem(blockEntity, matrices, vertexConsumers, light, overlay, beltInv.get(2), 0.5f, -getTransferCooldownOffset(blockEntity, 2) / 2);
            }

            if (!beltInv.get(3).isEmpty()) {
                renderBeltItem(blockEntity, matrices, vertexConsumers, light, overlay, beltInv.get(3), 0.5f, getTransferCooldownOffset(blockEntity, 3) / 2);
            }
        }
        */

        // Mandatory call after GL calls
        matrices.pop();
    }

    public float getTransferCooldownOffset(ConveyorBeltBlockEntity blockEntity, int slot){
        return 1f - ((float) blockEntity.transferCooldownCounter[slot] / (float) blockEntity.transferCooldown);
    }

    public void renderBeltItem(ConveyorBeltBlockEntity blockEntity, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay, ItemStack itemStack, float offset, float sidewaysOffset){
        Direction facing = blockEntity.getCachedState().get(ConveyorBelt.FACING);

        // Move the item
        Vec3f translated = new Vec3f();

        boolean hasDepth = Objects.requireNonNull(MinecraftClient.getInstance().getItemRenderer().getModels().getModel(itemStack.getItem())).hasDepth();

        float height = hasDepth ? 0.31f : 0.39f;
        float itemScale = 0.7f;

        Direction slopeDir = null;

        if(blockEntity instanceof SlopedConveyorBeltDownBlockEntity){
            height += 1f - offset;

            slopeDir = facing;
        }
        else if(blockEntity instanceof SlopedConveyorBeltUpBlockEntity){
            height += offset;

            slopeDir = facing.getOpposite();
        }

        if (facing == Direction.NORTH) {
            translated = new Vec3f(0.5f + sidewaysOffset, height, 1 - offset);
        } else if (facing == Direction.EAST) {
            translated = new Vec3f(offset, height, 0.5f + sidewaysOffset);
        } else if (facing == Direction.SOUTH) {
            translated = new Vec3f(0.5f - sidewaysOffset, height, offset);
        } else if (facing == Direction.WEST) {
            translated = new Vec3f(1 - offset, height, 0.5f - sidewaysOffset);
        }

        matrices.translate(translated.getX(), translated.getY(), translated.getZ());

        if(slopeDir == Direction.NORTH){
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(-45));
        }
        else if(slopeDir == Direction.SOUTH){
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(45));
        }
        else if(slopeDir == Direction.EAST){
            matrices.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(-45));
        }
        else if(slopeDir == Direction.WEST){
            matrices.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(45));
        }

        if(!hasDepth){
            matrices.translate(0f, 0f, -0.08f);
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(90));
            matrices.scale(itemScale, itemScale, itemScale);
        }

        MinecraftClient.getInstance().getItemRenderer().renderItem(itemStack, ModelTransformation.Mode.GROUND, light, overlay, matrices, vertexConsumers, 0);

        if(!hasDepth){
            matrices.scale(1/itemScale, 1/itemScale, 1/itemScale);
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(-90));
            matrices.translate(0f, 0f, 0.08f);
        }

        if(slopeDir == Direction.NORTH){
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(45));
        }
        else if(slopeDir == Direction.SOUTH){
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(-45));
        }
        else if(slopeDir == Direction.EAST){
            matrices.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(45));
        }
        else if(slopeDir == Direction.WEST){
            matrices.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(-45));
        }

        matrices.translate(-translated.getX(), -translated.getY(), -translated.getZ());
    }
}