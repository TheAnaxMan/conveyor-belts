package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public class RoboticArmScreen extends HandledScreen<RoboticArmScreenHandler> {
    public Identifier TEXTURE = new Identifier(ConveyorBelts.MOD_ID, "textures/gui/container/robotic_arm.png");

    public boolean insertSide;
    public boolean insertTop;

    public RoboticArmScreen(RoboticArmScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //Insert mode select

        textRenderer.draw(matrices, Text.of("Insert:"), x + 120, y + 27, 4210752);
        textRenderer.draw(matrices, Text.of("Side"), x + 131, y + 39, 4210752);
        textRenderer.draw(matrices, Text.of("Top"), x + 131, y + 51, 4210752);

        getInsertMode();//Do it each frame in-case another player is updating it also

        if(insertSide){
            textRenderer.draw(matrices, Text.of("x"), x + 121, y + 39, 0xffffff);
        }

        if(insertTop){
            textRenderer.draw(matrices, Text.of("x"), x + 121, y + 51, 0xffffff);
        }
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (client == null || client.player == null || client.player.isSpectator()) {
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        double mx = mouseX - x;
        double my = mouseY - y;

        if(mx >= 120 && mx <= 127){
            if(my >= 40 && my <= 47){
                insertSide = !insertSide;
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK, 0.3f, 1.0f);
                applyInsertMode();
            }

            if(my >= 52 && my <= 59){
                insertTop = !insertTop;
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK, 0.3f, 1.0f);
                applyInsertMode();
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    private void applyInsertMode(){
        int toSend = 0;
        if(insertSide && insertTop){
            toSend = RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP;
        }
        else if(insertSide){
            toSend = RoboticArmBlockEntity.INSERT_MODE_SIDE;
        }
        else if(insertTop){
            toSend = RoboticArmBlockEntity.INSERT_MODE_TOP;
        }
        else{
            toSend = RoboticArmBlockEntity.INSERT_MODE_NONE;
        }

        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeVarInt(toSend);
        ClientPlayNetworking.send(ConveyorBelts.UPDATE_ROBOTIC_ARM_INSERT_MODE_PACKET_ID, buf);
    }

    private void getInsertMode(){
        int insertMode = handler.getSelectedInsertMode();

        if(insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP){
            insertSide = true;
            insertTop = true;
        }
        else if(insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE){
            insertSide = true;
            insertTop = false;
        }
        else if(insertMode == RoboticArmBlockEntity.INSERT_MODE_TOP){
            insertSide = false;
            insertTop = true;
        }
        else{
            insertSide = false;
            insertTop = false;
        }
    }
}
