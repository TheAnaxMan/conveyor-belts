package uk.co.cablepost.conveyorbelts.robotic_arm.gold;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmBlockEntity;

public class GoldFilterRoboticArmBlockEntity extends FilterRoboticArmBlockEntity {
    public GoldFilterRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.GOLD_ROBOTIC_ARM_SPEED;
    }
}
