package uk.co.cablepost.conveyorbelts.robotic_arm.netherite;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmBlockEntity;

public class NetheriteFilterRoboticArmBlockEntity extends FilterRoboticArmBlockEntity {
    public NetheriteFilterRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.NETHERITE_ROBOTIC_ARM_SPEED;
        this.maxStackSize = 10000;
    }
}
