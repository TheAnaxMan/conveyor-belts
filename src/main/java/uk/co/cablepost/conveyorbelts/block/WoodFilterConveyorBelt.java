package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.WoodFilterConveyorBeltBlockEntity;

public class WoodFilterConveyorBelt extends FilterConveyorBelt{
    public WoodFilterConveyorBelt(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new WoodFilterConveyorBeltBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? checkType(type, ConveyorBelts.WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY, WoodFilterConveyorBeltBlockEntity::clientTick) : checkType(type, ConveyorBelts.WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY, WoodFilterConveyorBeltBlockEntity::serverTick);
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
    }
}
