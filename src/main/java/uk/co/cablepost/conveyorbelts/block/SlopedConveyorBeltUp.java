package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.*;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.client.ConveyorBeltsClient;

import java.util.Objects;

public class SlopedConveyorBeltUp extends ConveyorBelt {

    public static final EnumProperty<DoubleBlockHalf> HALF;

    static {
        HALF =  Properties.DOUBLE_BLOCK_HALF;
    }

    public SlopedConveyorBeltUp(AbstractBlock.Settings settings) {
        super(settings, true);
        this.setDefaultState(
                this.stateManager.getDefaultState()
                        .with(ENABLED, true)
                        .with(FACING, Direction.NORTH)
                        .with(HALF, DoubleBlockHalf.LOWER)
        );
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        super.appendProperties(builder);

        builder.add(
                HALF
        );
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return null;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return getOutlineShape2(state, state.get(FACING));
    }

    public VoxelShape getOutlineShape2(BlockState state, Direction direction) {
        float gradientMul = 1f;

        float step = 0.2f;

        VoxelShape shape = VoxelShapes.empty();

        DoubleBlockHalf doubleBlockHalf = state.get(HALF);

        if(doubleBlockHalf == DoubleBlockHalf.LOWER) {
            if (direction == Direction.NORTH) {
                for (float i = 0.0f; i < 1.0f; i += step) {
                    shape = VoxelShapes.union(shape, VoxelShapes.cuboid(
                            0.0f,
                            Math.max(0.0f, (1 - i) * gradientMul - 0.1f),
                            i,
                            1.0f,
                            Math.min(((1 - i) + 0.375f) * gradientMul, 1.0f),
                            i + step
                    ));
                }
            } else if (direction == Direction.SOUTH) {
                for (float i = 0.0f; i < 1.0f; i += step) {
                    shape = VoxelShapes.union(shape, VoxelShapes.cuboid(
                            0.0f,
                            i * gradientMul,
                            i,
                            1.0f,
                            Math.min((i + 0.375f + 0.1f) * gradientMul, 1.0f),
                            i + step
                    ));
                }
            } else if (direction == Direction.EAST) {
                for (float i = 0.0f; i < 1.0f; i += step) {
                    shape = VoxelShapes.union(shape, VoxelShapes.cuboid(
                            i,
                            i * gradientMul,
                            0.0f,
                            i + step,
                            Math.min((i + 0.375f + 0.1f) * gradientMul, 1.0f),
                            1.0f
                    ));
                }
            } else if (direction == Direction.WEST) {
                for (float i = 0.0f; i < 1.0f; i += step) {
                    shape = VoxelShapes.union(shape, VoxelShapes.cuboid(
                            i,
                            Math.max(0.0f, (1 - i) * gradientMul - 0.1f),
                            0.0f,
                            i + step,
                            Math.min(((1 - i) + 0.375f) * gradientMul, 1.0f),
                            1.0f
                    ));
                }
            }
        }
        else{
            return switch (direction) {
                case NORTH -> VoxelShapes.cuboid(0.0f, 0.0f, 0.0f, 1.0f, 0.375f, 0.3f);
                case SOUTH -> VoxelShapes.cuboid(0.0f, 0.0f, 0.7f, 1.0f, 0.375f, 1.0f);
                case EAST -> VoxelShapes.cuboid(0.7f, 0.0f, 0.0f, 1.0f, 0.375f, 1.0f);
                case WEST -> VoxelShapes.cuboid(0.0f, 0.0f, 0.0f, 0.3f, 0.375f, 1.0f);
                default -> VoxelShapes.fullCube();
            };
        }

        return shape.simplify();
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        DoubleBlockHalf doubleBlockHalf = state.get(HALF);
        if(doubleBlockHalf == DoubleBlockHalf.LOWER){
            return BlockRenderType.MODEL;
        }else{
            return BlockRenderType.INVISIBLE;
        }
    }

    //Yoinked from TallFlowerBlock

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
        BlockPos blockPos = pos.up();
        if (state.get(HALF) == DoubleBlockHalf.UPPER) {
            blockPos = pos.down();
        }
        world.setBlockState(
                blockPos,
                TallPlantBlock.withWaterloggedState(
                        world,
                        blockPos,
                        this.getDefaultState().with(
                                HALF,
                                getOtherHalfState(state.get(HALF))
                        ).with(
                                FACING,
                                state.get(FACING)
                        )
                ),
                Block.NOTIFY_ALL
        );
    }

    private static DoubleBlockHalf getOtherHalfState(DoubleBlockHalf half){
        if(half == DoubleBlockHalf.UPPER){
            return DoubleBlockHalf.LOWER;
        }
        return DoubleBlockHalf.UPPER;
    }

    @Override
    public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos) {
        BlockState blockState;
        if (state.get(HALF) == DoubleBlockHalf.UPPER) {
            blockState = world.getBlockState(pos.down());
        }
        else{
            blockState = world.getBlockState(pos.up());
        }

        if(blockState.isOf(this)){
            if(blockState.get(HALF) != state.get(HALF)){
                return super.canPlaceAt(state, world, pos);
            }
        }

        if(!blockState.isAir()){
            return false;
        }

        return super.canPlaceAt(state, world, pos);
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        if (!world.isClient) {
            if (player.isCreative()) {
                onBreakInCreative(world, pos, state, player);
            } else {
                TallPlantBlock.dropStacks(state, world, pos, null, player, player.getMainHandStack());
            }
        }
        super.onBreak(world, pos, state, player);
    }

    @Override
    public void afterBreak(World world, PlayerEntity player, BlockPos pos, BlockState state, @Nullable BlockEntity blockEntity, ItemStack stack) {
        super.afterBreak(world, player, pos, Blocks.AIR.getDefaultState(), blockEntity, stack);
    }

    /**
     * Destroys a bottom half of a tall double block (such as a plant or a door)
     * without dropping an item when broken in creative.
     *
     * @see Block#onBreak(World, BlockPos, BlockState, PlayerEntity)
     */
    protected static void onBreakInCreative(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        BlockPos blockPos;
        BlockState blockState;
        DoubleBlockHalf doubleBlockHalf = state.get(HALF);
        if (doubleBlockHalf == DoubleBlockHalf.UPPER && (blockState = world.getBlockState(blockPos = pos.down())).isOf(state.getBlock()) && blockState.get(HALF) == DoubleBlockHalf.LOWER) {
            BlockState blockState2 = blockState.contains(Properties.WATERLOGGED) && blockState.get(Properties.WATERLOGGED) != false ? Blocks.WATER.getDefaultState() : Blocks.AIR.getDefaultState();
            world.setBlockState(blockPos, blockState2, Block.NOTIFY_ALL | Block.SKIP_DROPS);
            world.syncWorldEvent(player, WorldEvents.BLOCK_BROKEN, blockPos, Block.getRawIdFromState(blockState));
        }
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState, WorldAccess world, BlockPos pos, BlockPos neighborPos) {
        DoubleBlockHalf doubleBlockHalf = state.get(HALF);

        if(neighborState.isOf(this)) {
            if (
                    (doubleBlockHalf == DoubleBlockHalf.LOWER && direction == Direction.UP) ||
                    (doubleBlockHalf == DoubleBlockHalf.UPPER && direction == Direction.DOWN)
            ) {
                //Is update from other half
                if(state.get(FACING) != neighborState.get(FACING)) {
                    return state.with(FACING, neighborState.get(FACING));//If other half got rotated, update this half too
                }
            }
        }

        if (!(direction.getAxis() != Direction.Axis.Y || doubleBlockHalf == DoubleBlockHalf.LOWER != (direction == Direction.UP) || neighborState.isOf(this) && neighborState.get(HALF) != doubleBlockHalf)) {
            return Blocks.AIR.getDefaultState();
        }
        if (doubleBlockHalf == DoubleBlockHalf.LOWER && direction == Direction.DOWN && !state.canPlaceAt(world, pos)) {
            return Blocks.AIR.getDefaultState();
        }
        return super.getStateForNeighborUpdate(state, direction, neighborState, world, pos, neighborPos);
    }

    @Override
    public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity) {
        DoubleBlockHalf doubleBlockHalf = state.get(HALF);

        if(doubleBlockHalf == DoubleBlockHalf.LOWER){
            super.onSteppedOn(world, pos, state, entity);
            return;
        }

        boolean bottomHalfEnabled = false;
        try {
            bottomHalfEnabled = world.getBlockState(pos.add(0, -1, 0)).get(ENABLED);
        }
        catch(java.lang.IllegalArgumentException ignored){
        }

        world.setBlockState(pos, state.with(ENABLED, bottomHalfEnabled));

        super.onSteppedOn(world, pos, state, entity);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        Direction side = ctx.getSide();
        PlayerEntity player = ctx.getPlayer();

        if(player == null){
            return this.getDefaultState();
        }

        boolean sneaking = false;//player.isSneaking();
        if (ctx.getWorld().isClient) {
            sneaking = ConveyorBeltsClient.invertBeltPlacement;
        }
        else{
            Boolean getInv = ConveyorBelts.invertBeltPlacementOfPlayer.get(player.getUuid());
            sneaking = getInv != null && getInv;
        }

        if((side.getOffsetX() != 0 || side.getOffsetZ() != 0) && side.getOffsetY() == 0 && sneaking){
            return super.getPlacementState(ctx).with(HALF, DoubleBlockHalf.UPPER);
        }

        return super.getPlacementState(ctx);
    }
}