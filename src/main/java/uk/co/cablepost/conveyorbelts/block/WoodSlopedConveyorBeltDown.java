package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.WoodSlopedConveyorBeltDownBlockEntity;

public class WoodSlopedConveyorBeltDown extends SlopedConveyorBeltDown {
    public WoodSlopedConveyorBeltDown(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new WoodSlopedConveyorBeltDownBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? checkType(type, ConveyorBelts.WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, WoodSlopedConveyorBeltDownBlockEntity::clientTick) : checkType(type, ConveyorBelts.WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, WoodSlopedConveyorBeltDownBlockEntity::serverTick);
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
        vector = vector.multiply(ConveyorBelts.WOOD_ENTITY_MOVE_SPEED);
        moveEntityOn2(vector, livingEntity);
    }

    public Identifier getNextCycleBeltType(){
        return ConveyorBelts.WOOD_CONVEYOR_BELT_IDENTIFIER;
    }
}
