package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public class GoldConveyorBeltBlockEntity extends ConveyorBeltBlockEntity {
    public GoldConveyorBeltBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.GOLD_CONVEYOR_BELT_BLOCK_ENTITY);
        this.transferCooldown = ConveyorBelts.GOLD_TRANSFER_COOLDOWN;
        this.moveToCenterSpeed = ConveyorBelts.GOLD_MOVE_TO_CENTER_SPEED;
    }
}
