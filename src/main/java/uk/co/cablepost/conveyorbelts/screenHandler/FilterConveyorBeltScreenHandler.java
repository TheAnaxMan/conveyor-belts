package uk.co.cablepost.conveyorbelts.screenHandler;

import dev.latvian.mods.itemfilters.item.BaseFilterItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.util.ItemFilters;

import java.util.Objects;

public class FilterConveyorBeltScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    //This constructor gets called on the client when the server wants it to open the screenHandler,
    //The client will call the other constructor with an empty Inventory and the screenHandler will automatically
    //sync this empty inventory with the inventory on the server.
    public FilterConveyorBeltScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, 6);
    }

    public FilterConveyorBeltScreenHandler(int syncId, PlayerInventory playerInventory, int size) {
        this(syncId, playerInventory, new SimpleInventory(size), new ArrayPropertyDelegate(FilterConveyorBeltBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    //This constructor gets called from the BlockEntity on the server without calling the other constructor first, the server knows the inventory of the container
    //and can therefore directly provide it as an argument. This inventory will then be synced to the client.
    public FilterConveyorBeltScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(ConveyorBelts.FILTER_CONVEYOR_BELT_SCREEN_HANDLER, syncId);
        checkSize(inventory, 6);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);
        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        //This will place the slot in the correct locations. The slots exist on both server and client!
        //This will not render the background of the slots however, this is the Screens job
        //Our inventory
        this.addSlot(new Slot(inventory, 0, 62 + 18 * 1, 17 + 18 * 2 + 8));
        this.addSlot(new Slot(inventory, 1, 62 + 18 * 1, 17 + 18 * 1 + 8));
        this.addSlot(new Slot(inventory, 2, 62 + 18 *-1, 17 + 18 * 0));
        this.addSlot(new Slot(inventory, 3, 62 + 18 * 3, 17 + 18 * 0));
        this.addSlot(new Slot(inventory, 4, 62 + 18 *-1, 17 + 18 * 1 + 4));
        this.addSlot(new Slot(inventory, 5, 62 + 18 * 3, 17 + 18 * 1 + 4));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 84 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 142));
        }

    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {

        Slot slot = this.slots.get(invSlot);

        if(invSlot == 4 || invSlot == 5){
            ItemStack stack = slot.getStack();
            if(stack.isEmpty()){
                return ItemStack.EMPTY;
            }
            NbtCompound n = stack.getOrCreateNbt();
            if(n.getBoolean("IsFilterItem")){//If they put the item in prior to this update this will be false
                return ItemStack.EMPTY;
            }
        }

        ItemStack newStack = ItemStack.EMPTY;
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size() - 2, false)) {//-2 so cant go into last 2 filter slots
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    @Override
    public void onSlotClick(int slotIndex, int button, SlotActionType actionType, PlayerEntity player) {

        boolean doFilterSet = false;

        if(slotIndex == 4 || slotIndex == 5){
            Slot slot = this.slots.get(slotIndex);

            ItemStack oldStack = slot.getStack();
            if(oldStack.isEmpty()){
                doFilterSet = true;
            }
            NbtCompound oldNbt = oldStack.getOrCreateNbt();
            if(oldNbt.getBoolean("IsFilterItem")){
                doFilterSet = true;
            }
        }

        if(doFilterSet){
            Slot slot = this.slots.get(slotIndex);

            ItemStack newStack = getCursorStack().copy();
            //String namespace = Registry.ITEM.getId(newStack.getItem()).getNamespace();

            //if(!Objects.equals(namespace, "itemfilters") && !Objects.equals(namespace, "bbfilters")) {//Filter items keep their NBT
            if(!ItemFilters.IsFilterItem(newStack)) {//Filter items keep their NBT
                newStack = new ItemStack(newStack.getItem());//Using the item instead of the item stack to show the user this ignores things like damage, stack size and enchants - for non filter items
            }

            NbtCompound newNbt = newStack.getOrCreateNbt();
            newNbt.putBoolean("IsFilterItem", true);
            newStack.writeNbt(newNbt);
            slot.setStack(newStack);

            slot.markDirty();
        }
        else {
            super.onSlotClick(slotIndex, button, actionType, player);
        }
    }

    public void setOutputDirectionsMode(int outputDirectionsMode){
        this.propertyDelegate.set(0, outputDirectionsMode);
    }

    public int getOutputDirectionsMode(){
        return this.propertyDelegate.get(0);
    }
}
