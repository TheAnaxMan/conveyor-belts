package uk.co.cablepost.conveyorbelts.util;

import dev.latvian.mods.itemfilters.api.ItemFiltersAPI;
import dev.latvian.mods.itemfilters.item.BaseFilterItem;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.item.ItemStack;

public class ItemFilters {
    public static boolean Filter(ItemStack filterStack, ItemStack inputStack){
        if(FabricLoader.getInstance().isModLoaded("itemfilters")){
            return
                    !filterStack.isEmpty() &&
                    ItemFiltersAPI.filter(filterStack, inputStack)
            ;
        }

        return inputStack.isItemEqual(filterStack);
    }

    public static boolean IsFilterItem(ItemStack stack){
        if(!FabricLoader.getInstance().isModLoaded("itemfilters")){
            return false;
        }

        return stack.getItem() instanceof BaseFilterItem;
    }
}
