package uk.co.cablepost.conveyorbelts;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.conveyorbelts.block.*;
import uk.co.cablepost.conveyorbelts.blockEntity.*;
import uk.co.cablepost.conveyorbelts.blockItem.ConveyorBeltBockItem;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreenHandler;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmScreenHandler;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.screenHandler.ConveyorBeltScreenHandler;
import uk.co.cablepost.conveyorbelts.screenHandler.FilterConveyorBeltScreenHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ConveyorBelts implements ModInitializer {
    public static final String MOD_ID = "conveyorbelts";

    public static Map<UUID, Boolean> invertBeltPlacementOfPlayer = new HashMap<>();
    public static final Identifier UPDATE_INVERT_BELT_PLACEMENT_PACKET_ID = new Identifier(MOD_ID, "update_invert_belt_placement");
    public static final Identifier CYCLE_BELT_ELEVATION_TYPE_PACKET_ID = new Identifier(MOD_ID, "cycle_belt_elevation_type");

    public static final Identifier UPDATE_ROBOTIC_ARM_INSERT_MODE_PACKET_ID = new Identifier(MOD_ID, "update_robotic_arm_insert_mode");
    public static final Identifier UPDATE_FILTER_CONVEYOR_BELT_OUTPUT_DIRECTIONS_MODE_PACKET_ID = new Identifier(MOD_ID, "update_filter_conveyor_belt_output_directions_mode");

    //BELT TRANSFER COOL DOWNS
    public static final int WOOD_TRANSFER_COOLDOWN = 30;
    public static final int IRON_TRANSFER_COOLDOWN = 4;
    public static final int GOLD_TRANSFER_COOLDOWN = 2;

    //BELT ENTITY MOVE SPEEDS
    public static final double WOOD_ENTITY_MOVE_SPEED = 0.011f;
    public static final double IRON_ENTITY_MOVE_SPEED = 0.108f;
    public static final double GOLD_ENTITY_MOVE_SPEED = 0.375f;

    //BELT MOVE ITEMS TO CENTER SPEED
    public static final int WOOD_MOVE_TO_CENTER_SPEED = 2;
    public static final int IRON_MOVE_TO_CENTER_SPEED = 5;
    public static final int GOLD_MOVE_TO_CENTER_SPEED = 12;

    //ROBOTIC ARM MAX PROGRESS (SPEEDS)
    public static final int WOOD_ROBOTIC_ARM_SPEED = 100;
    public static final int IRON_ROBOTIC_ARM_SPEED = 10;
    public static final int GOLD_ROBOTIC_ARM_SPEED = 6;
    public static final int NETHERITE_ROBOTIC_ARM_SPEED = 6;

    //BELT BLOCKS

    //==== REGULAR ====

    //wood
    public WoodConveyorBelt WOOD_CONVEYOR_BELT = new WoodConveyorBelt(FabricBlockSettings.of(Material.WOOD).strength(0.8f));
    public static final Identifier WOOD_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "wood_conveyor_belt");
    public static BlockEntityType<WoodConveyorBeltBlockEntity> WOOD_CONVEYOR_BELT_BLOCK_ENTITY;

    //iron
    public static IronConveyorBelt IRON_CONVEYOR_BELT = new IronConveyorBelt(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier IRON_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "iron_conveyor_belt");
    public static BlockEntityType<IronConveyorBeltBlockEntity> IRON_CONVEYOR_BELT_BLOCK_ENTITY;

    //gold
    public GoldConveyorBelt GOLD_CONVEYOR_BELT = new GoldConveyorBelt(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier GOLD_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "gold_conveyor_belt");
    public static BlockEntityType<GoldConveyorBeltBlockEntity> GOLD_CONVEYOR_BELT_BLOCK_ENTITY;

    //==== SLOPED UP ====

    //wood
    public WoodSlopedConveyorBeltUp WOOD_SLOPED_CONVEYOR_BELT_UP = new WoodSlopedConveyorBeltUp(FabricBlockSettings.of(Material.WOOD).strength(0.8f));
    public static final Identifier WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = new Identifier(MOD_ID, "wood_sloped_conveyor_belt_up");
    public static BlockEntityType<WoodSlopedConveyorBeltUpBlockEntity> WOOD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //iron
    public IronSlopedConveyorBeltUp IRON_SLOPED_CONVEYOR_BELT_UP = new IronSlopedConveyorBeltUp(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = new Identifier(MOD_ID, "iron_sloped_conveyor_belt_up");
    public static BlockEntityType<IronSlopedConveyorBeltUpBlockEntity> IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //gold
    public GoldSlopedConveyorBeltUp GOLD_SLOPED_CONVEYOR_BELT_UP = new GoldSlopedConveyorBeltUp(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = new Identifier(MOD_ID, "gold_sloped_conveyor_belt_up");
    public static BlockEntityType<GoldSlopedConveyorBeltUpBlockEntity> GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //==== SLOPED DOWN ====

    //wood
    public WoodSlopedConveyorBeltDown WOOD_SLOPED_CONVEYOR_BELT_DOWN = new WoodSlopedConveyorBeltDown(FabricBlockSettings.of(Material.WOOD).strength(0.8f));
    public static final Identifier WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = new Identifier(MOD_ID, "wood_sloped_conveyor_belt_down");
    public static BlockEntityType<WoodSlopedConveyorBeltDownBlockEntity> WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //iron
    public IronSlopedConveyorBeltDown IRON_SLOPED_CONVEYOR_BELT_DOWN = new IronSlopedConveyorBeltDown(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = new Identifier(MOD_ID, "iron_sloped_conveyor_belt_down");
    public static BlockEntityType<IronSlopedConveyorBeltDownBlockEntity> IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //gold
    public GoldSlopedConveyorBeltDown GOLD_SLOPED_CONVEYOR_BELT_DOWN = new GoldSlopedConveyorBeltDown(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = new Identifier(MOD_ID, "gold_sloped_conveyor_belt_down");
    public static BlockEntityType<GoldSlopedConveyorBeltDownBlockEntity> GOLD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //==== FILTER ====

    //wood
    public WoodFilterConveyorBelt WOOD_FILTER_CONVEYOR_BELT = new WoodFilterConveyorBelt(FabricBlockSettings.of(Material.WOOD).strength(0.8f));
    public static final Identifier WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "wood_filter_conveyor_belt");
    public static BlockEntityType<WoodFilterConveyorBeltBlockEntity> WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //iron
    public IronFilterConveyorBelt IRON_FILTER_CONVEYOR_BELT = new IronFilterConveyorBelt(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier IRON_FILTER_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "iron_filter_conveyor_belt");
    public static BlockEntityType<IronFilterConveyorBeltBlockEntity> IRON_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //gold
    public GoldFilterConveyorBelt GOLD_FILTER_CONVEYOR_BELT = new GoldFilterConveyorBelt(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static final Identifier GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER = new Identifier(MOD_ID, "gold_filter_conveyor_belt");
    public static BlockEntityType<GoldFilterConveyorBeltBlockEntity> GOLD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //BELT SCREENS
    public static final ScreenHandlerType<ConveyorBeltScreenHandler> CONVEYOR_BELT_SCREEN_HANDLER = Registry.register(Registry.SCREEN_HANDLER, new Identifier(MOD_ID, "conveyor_belt_screen"), new ScreenHandlerType<>(ConveyorBeltScreenHandler::new));
    public static final ScreenHandlerType<FilterConveyorBeltScreenHandler> FILTER_CONVEYOR_BELT_SCREEN_HANDLER = Registry.register(Registry.SCREEN_HANDLER, new Identifier(MOD_ID, "filter_conveyor_belt_screen"), new ScreenHandlerType<>(FilterConveyorBeltScreenHandler::new));

    //=== ROBOTIC ARMS ===
    //--- Wood ---
    public WoodRoboticArmBlock WOOD_ROBOTIC_ARM = new WoodRoboticArmBlock(FabricBlockSettings.of(Material.WOOD).strength(1.4f));
    public static final Identifier WOOD_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "wood_robotic_arm");
    public static BlockEntityType<WoodRoboticArmBlockEntity> WOOD_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Iron ---
    public IronRoboticArmBlock IRON_ROBOTIC_ARM = new IronRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier IRON_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "iron_robotic_arm");
    public static BlockEntityType<IronRoboticArmBlockEntity> IRON_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Gold ---
    public GoldRoboticArmBlock GOLD_ROBOTIC_ARM = new GoldRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier GOLD_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "gold_robotic_arm");
    public static BlockEntityType<GoldRoboticArmBlockEntity> GOLD_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Netherite ---
    public NetheriteRoboticArmBlock NETHERITE_ROBOTIC_ARM = new NetheriteRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier NETHERITE_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "netherite_robotic_arm");
    public static BlockEntityType<NetheriteRoboticArmBlockEntity> NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Wood Filter ---
    public WoodFilterRoboticArmBlock WOOD_FILTER_ROBOTIC_ARM = new WoodFilterRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "wood_filter_robotic_arm");
    public static BlockEntityType<WoodFilterRoboticArmBlockEntity> WOOD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Iron Filter ---
    public IronFilterRoboticArmBlock IRON_FILTER_ROBOTIC_ARM = new IronFilterRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier IRON_FILTER_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "iron_filter_robotic_arm");
    public static BlockEntityType<IronFilterRoboticArmBlockEntity> IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Gold Filter ---
    public GoldFilterRoboticArmBlock GOLD_FILTER_ROBOTIC_ARM = new GoldFilterRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "gold_filter_robotic_arm");
    public static BlockEntityType<GoldFilterRoboticArmBlockEntity> GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Netherite Filter ---
    public NetheriteFilterRoboticArmBlock NETHERITE_FILTER_ROBOTIC_ARM = new NetheriteFilterRoboticArmBlock(FabricBlockSettings.of(Material.METAL).strength(1.4f));
    public static final Identifier NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER = new Identifier(MOD_ID, "netherite_filter_robotic_arm");
    public static BlockEntityType<NetheriteFilterRoboticArmBlockEntity> NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Screens ---
    public static final ScreenHandlerType<RoboticArmScreenHandler> ROBOTIC_ARM_SCREEN_HANDLER = Registry.register(Registry.SCREEN_HANDLER, new Identifier(MOD_ID, "robotic_arm_screen"), new ScreenHandlerType<>(RoboticArmScreenHandler::new));
    public static final ScreenHandlerType<FilterRoboticArmScreenHandler> FILTER_ROBOTIC_ARM_SCREEN_HANDLER = Registry.register(Registry.SCREEN_HANDLER, new Identifier(MOD_ID, "filter_robotic_arm_screen"), new ScreenHandlerType<>(FilterRoboticArmScreenHandler::new));


    public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build(
            new Identifier(MOD_ID, "items"),
            () -> new ItemStack(IRON_CONVEYOR_BELT)
    );


    @Override
    public void onInitialize() {

        //==== REGULAR ====

        //wood

        WOOD_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodConveyorBeltBlockEntity::new,
                        WOOD_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registry.BLOCK, WOOD_CONVEYOR_BELT_IDENTIFIER, WOOD_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, WOOD_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(WOOD_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));

        //iron

        IRON_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronConveyorBeltBlockEntity::new,
                        IRON_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registry.BLOCK, IRON_CONVEYOR_BELT_IDENTIFIER, IRON_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, IRON_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(IRON_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));

        //gold

        GOLD_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldConveyorBeltBlockEntity::new,
                        GOLD_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registry.BLOCK, GOLD_CONVEYOR_BELT_IDENTIFIER, GOLD_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, GOLD_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(GOLD_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));


        //==== SLOPED UP ====

        //wood

        WOOD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodSlopedConveyorBeltUpBlockEntity::new,
                        WOOD_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registry.BLOCK, WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, WOOD_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registry.ITEM, WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(WOOD_SLOPED_CONVEYOR_BELT_UP, new FabricItemSettings().group(ITEM_GROUP)));

        //iron

        IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronSlopedConveyorBeltUpBlockEntity::new,
                        IRON_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registry.BLOCK, IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, IRON_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registry.ITEM, IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(IRON_SLOPED_CONVEYOR_BELT_UP, new FabricItemSettings().group(ITEM_GROUP)));

        //gold

        GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldSlopedConveyorBeltUpBlockEntity::new,
                        GOLD_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registry.BLOCK, GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, GOLD_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registry.ITEM, GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(GOLD_SLOPED_CONVEYOR_BELT_UP, new FabricItemSettings().group(ITEM_GROUP)));

        //==== SLOPED DOWN ====

        //wood

        WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodSlopedConveyorBeltDownBlockEntity::new,
                        WOOD_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registry.BLOCK, WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, WOOD_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registry.ITEM, WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(WOOD_SLOPED_CONVEYOR_BELT_DOWN, new FabricItemSettings().group(ITEM_GROUP)));

        //iron

        IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronSlopedConveyorBeltDownBlockEntity::new,
                        IRON_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registry.BLOCK, IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, IRON_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registry.ITEM, IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(IRON_SLOPED_CONVEYOR_BELT_DOWN, new FabricItemSettings().group(ITEM_GROUP)));

        //gold

        GOLD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldSlopedConveyorBeltDownBlockEntity::new,
                        GOLD_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registry.BLOCK, GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, GOLD_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registry.ITEM, GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(GOLD_SLOPED_CONVEYOR_BELT_DOWN, new FabricItemSettings().group(ITEM_GROUP)));

        //==== FILTER ====

        //wood

        WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodFilterConveyorBeltBlockEntity::new,
                        WOOD_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registry.BLOCK, WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER, WOOD_FILTER_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(WOOD_FILTER_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));

        //iron

        IRON_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronFilterConveyorBeltBlockEntity::new,
                        IRON_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registry.BLOCK, IRON_FILTER_CONVEYOR_BELT_IDENTIFIER, IRON_FILTER_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, IRON_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(IRON_FILTER_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));

        //gold

        GOLD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldFilterConveyorBeltBlockEntity::new,
                        GOLD_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registry.BLOCK, GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER, GOLD_FILTER_CONVEYOR_BELT);
        Registry.register(Registry.ITEM, GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(GOLD_FILTER_CONVEYOR_BELT, new FabricItemSettings().group(ITEM_GROUP)));

        //=== ROBOTIC ARM ===

        //Wood
        WOOD_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodRoboticArmBlockEntity::new,
                        WOOD_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, WOOD_ROBOTIC_ARM_IDENTIFIER, WOOD_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, WOOD_ROBOTIC_ARM_IDENTIFIER, new BlockItem(WOOD_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Iron
        IRON_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronRoboticArmBlockEntity::new,
                        IRON_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, IRON_ROBOTIC_ARM_IDENTIFIER, IRON_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, IRON_ROBOTIC_ARM_IDENTIFIER, new BlockItem(IRON_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Gold
        GOLD_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldRoboticArmBlockEntity::new,
                        GOLD_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, GOLD_ROBOTIC_ARM_IDENTIFIER, GOLD_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, GOLD_ROBOTIC_ARM_IDENTIFIER, new BlockItem(GOLD_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Netherite
        NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                NETHERITE_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteRoboticArmBlockEntity::new,
                        NETHERITE_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, NETHERITE_ROBOTIC_ARM_IDENTIFIER, NETHERITE_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, NETHERITE_ROBOTIC_ARM_IDENTIFIER, new BlockItem(NETHERITE_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Wood Filter
        WOOD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodFilterRoboticArmBlockEntity::new,
                        WOOD_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER, WOOD_FILTER_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(WOOD_FILTER_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Iron Filter
        IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                IRON_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronFilterRoboticArmBlockEntity::new,
                        IRON_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, IRON_FILTER_ROBOTIC_ARM_IDENTIFIER, IRON_FILTER_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, IRON_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(IRON_FILTER_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Gold Filter
        GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldFilterRoboticArmBlockEntity::new,
                        GOLD_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER, GOLD_FILTER_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(GOLD_FILTER_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //Netherite Filter
        NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteFilterRoboticArmBlockEntity::new,
                        NETHERITE_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registry.BLOCK, NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER, NETHERITE_FILTER_ROBOTIC_ARM);
        Registry.register(Registry.ITEM, NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(NETHERITE_FILTER_ROBOTIC_ARM, new FabricItemSettings().group(ITEM_GROUP)));

        //=== PACKETS ===

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_INVERT_BELT_PLACEMENT_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            boolean invert = buf.readBoolean();
            boolean sendChatMessage = buf.readBoolean();

            server.executeSync(() -> {
                invertBeltPlacementOfPlayer.put(player.getUuid(), invert);
                if(sendChatMessage) {
                    if (invert) {
                        player.sendMessage(Text.translatable("text.conveyorbelts.belt_will_place_away.message").formatted(Formatting.GRAY), false);
                    } else {
                        player.sendMessage(Text.translatable("text.conveyorbelts.belt_will_place_towards.message").formatted(Formatting.GRAY), false);
                    }
                }
            });
        });

        ServerPlayNetworking.registerGlobalReceiver(CYCLE_BELT_ELEVATION_TYPE_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            server.executeSync(() -> {
                ItemStack stack = player.getStackInHand(Hand.MAIN_HAND);

                if(stack.getItem() instanceof ConveyorBeltBockItem i){
                    if(i.getBlock() instanceof ConveyorBelt b){
                        Identifier next = b.getNextCycleBeltType();
                        if(next != null) {
                            player.setStackInHand(Hand.MAIN_HAND, new ItemStack(Registry.ITEM.get(next), stack.getCount()));
                        }
                    }
                }
            });
        });

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_ROBOTIC_ARM_INSERT_MODE_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            int insertMode = buf.readVarInt();

            server.executeSync(() -> {
                if (!player.isSpectator() && player.currentScreenHandler instanceof RoboticArmScreenHandler screenHandler) {
                    screenHandler.setSelectedInsertMode(insertMode);
                }
            });
        });

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_FILTER_CONVEYOR_BELT_OUTPUT_DIRECTIONS_MODE_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            int outputDirectionsMode = buf.readVarInt();

            server.executeSync(() -> {
                if (!player.isSpectator() && player.currentScreenHandler instanceof FilterConveyorBeltScreenHandler screenHandler) {
                    screenHandler.setOutputDirectionsMode(outputDirectionsMode);
                }
            });
        });
    }
}
